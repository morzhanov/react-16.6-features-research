import React, { Component } from "react";
import { BrowserRouter, Switch, Route, Link } from "react-router-dom";
import StrictMode from "./features/StrictMode";
import ErrorBoundary from "./features/ErrorBoundary";
import Context from "./features/Context";
import Memo from "./features/Memo";
import Suspence from "./features/Suspence";
import "./App.css";

const Header = () => (
  <header>
    <nav>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/strict">Strict</Link>
        </li>
        <li>
          <Link to="/error">Error Boundary</Link>
        </li>
        <li>
          <Link to="/context">Context</Link>
        </li>
        <li>
          <Link to="/memo">Memo</Link>
        </li>
        <li>
          <Link to="/suspence">Suspence</Link>
        </li>
      </ul>
    </nav>
  </header>
);

const Home = () => <main>Home page</main>;

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <>
          <Header />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/strict" component={StrictMode} />
            <Route path="/error" component={ErrorBoundary} />
            <Route path="/context" component={Context} />
            <Route path="/memo" component={Memo} />
            <Route path="/suspence" component={Suspence} />
          </Switch>
        </>
      </BrowserRouter>
    );
  }
}

export default App;
