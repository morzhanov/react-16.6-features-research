import React from "react";

class ComponentOne extends React.Component {
  componentWillMount() {
    // will cause deprecation error
  }

  render() {
    return <div>Component One</div>;
  }
}

const ComponentTwo = ({ children }, context) => <div>Component Two</div>;
// will cause deprecation error
ComponentTwo.contextTypes = {};

// will console.log error in component one because of deprecated lifecycle method
// will console.log error in component two because of deprecated old context api
const StrictMode = () => (
  <React.StrictMode>
    <main>
      <ComponentOne />
      <ComponentTwo />
    </main>
  </React.StrictMode>
);

export default StrictMode;
