import React from "react";

/*
*
* NOTICE: this example will only work in production mode
*
*/

// with getDerivedStateFromError
class ErrorBoundaryOne extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <h1>Something went wrong.</h1>;
    }

    return this.props.children;
  }
}

// with componentDidCatch
class ErrorBoundaryTwo extends React.Component {
  state = { error: null, errorInfo: null };

  componentDidCatch(error, errorInfo) {
    this.setState({
      error: error,
      errorInfo: errorInfo
    });
  }

  render() {
    if (this.state.errorInfo) {
      return (
        <div>
          <h2>Something went wrong.</h2>
          <details style={{ whiteSpace: "pre-wrap" }}>
            {this.state.error && this.state.error.toString()}
            <br />
            {this.state.errorInfo.componentStack}
          </details>
        </div>
      );
    }

    return this.props.children;
  }
}

class Buggy extends React.Component {
  state = { greeting: "Welcome" };
  componentDidMount() {
    throw new Error("An error has occured in Buggy component!");
  }
  render() {
    return <h2>{this.state.greeting}</h2>;
  }
}

export default class ErrorBoundaryDemo extends React.Component {
  render() {
    return (
      <>
        <ErrorBoundaryOne>
          <Buggy />
        </ErrorBoundaryOne>
        <ErrorBoundaryTwo>
          <Buggy />
        </ErrorBoundaryTwo>
      </>
    );
  }
}
