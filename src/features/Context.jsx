import React from "react";

const ValueContext = React.createContext();

class ContextUser extends React.Component {
  // new way to consume a context without ValueContext.Consumer
  static contextType = ValueContext;
  componentDidMount() {
    let value = this.context;
    /* perform a side-effect at mount using the value of MyContext */
  }
  componentDidUpdate() {
    let value = this.context;
    /* ... */
  }
  componentWillUnmount() {
    let value = this.context;
    /* ... */
  }
  render() {
    let value = this.context;
    return <main>{value}</main>;
  }
}

export default class ContextDemo extends React.Component {
  render() {
    return (
      <ValueContext.Provider value={"value from context = 100"}>
        <ContextUser />
      </ValueContext.Provider>
    );
  }
}
