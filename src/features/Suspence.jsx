import React, { lazy, Suspense } from "react";
const SuspenceChild = lazy(() => import("./SuspenceChild"));

const SuspenceDemo = () => {
  return (
    <Suspense fallback={<main>Loading...</main>}>
      <SuspenceChild />
    </Suspense>
  );
};

export default SuspenceDemo;
